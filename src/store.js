import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'
import { userLoginReducer } from './reducers/userReducers'
import { todoReducer } from './reducers/todoReducers'

const reducer = combineReducers({
    userLogin: userLoginReducer,
    todo:todoReducer
})
const middleware = [thunk]
const userInfoFromStorage = localStorage.getItem('user') ? localStorage.getItem('user') : null
const todoFromStorage = localStorage.getItem('todo') ? JSON.parse(localStorage.getItem('todo')) : null
const initialState = {
    userLogin: { loading:false, error:null, userInfo: userInfoFromStorage },
    todo: todoFromStorage
}
const store = createStore(reducer, initialState,applyMiddleware(...middleware))

export default store;