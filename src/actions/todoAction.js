import { DATA_CREATE, DATA_UPDATE,DATA_REMOVE } from "../constants/todoConstants";
export const createTodo = (title, duedate, status) => async (dispatch) => {
    try {
        let payload = {
            title: title,
            duedate: duedate,
            status: status
        }
        await dispatch({
            type: DATA_CREATE,
            payload
        })
    } catch (error) {
        console.log("error",error)
    }
}


export const deleteTodo = (title) => async (dispatch) => {
    try {
        console.log("delete")
        await dispatch({
            type: DATA_REMOVE,
            payload:title
        })
    } catch (error) {
        console.log("error",error)
    }
}

export const updateTodo = (index,title,duedate,status) => async (dispatch) => {
    try {
        console.log("update")
        let payload = {
            index:index,
            title:title,
            duedate:duedate,
            status:status
        }
        await dispatch({
            type: DATA_UPDATE,
            payload:payload
        })
    } catch (error) {
       console.log("error",error)
    }
}

