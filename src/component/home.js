
import { useDispatch, useSelector } from 'react-redux'
import { useState, useEffect } from 'react'
import { Modal, Button, Form, } from 'react-bootstrap'
import { login } from './../actions/userAction'
import { createTodo, updateTodo } from './../actions/todoAction'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import { faplus } from '@fortawesome/free-solid-svg-icons'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import SingleTodo from "./singleTodo"
const Home = () => {
    const [show, setShow] = useState(false);
    const [index, setIndex] = useState("")
    const [title, setTitle] = useState("")
    const [isCreate, setIsCreate] = useState(true)
    const [dueDate, setDueDate] = useState("")
    const [status, setStatus] = useState("open")


    const clearData = () => {
        setTitle("")
        setIsCreate(true)
        setDueDate("")
        setStatus("open")
        setIndex("")
    }

    const dispatch = useDispatch()
    const submitTodo = async () => {
        setShow(false)
        if (isCreate) {
            await dispatch(createTodo(title, dueDate, status))
        } else {
            await dispatch(updateTodo(index, title, dueDate, status))
        }
        clearData()
    };
    const handleShow = () => {
        if (isCreate) {
            clearData()
        }
        setShow(true);
    }
    const handleClose = () => {
        setShow(false)
        setIsCreate(true)
    };

    const listTodo = useSelector(state => state.todo)
    const userLogin = useSelector(state => state.userLogin)
    const { userInfo } = userLogin
    console.log("userLogin", userLogin)
    const addTodo = () => {
        alert("add")
    }

    const selectTodo = (data, index) => {
        setTitle(data.title)
        setDueDate(data.duedate)
        setShow(true)
        setIsCreate(false)
        setIndex(index)
    }

    return (
        <>
            <Modal show={show} onHide={() => handleClose()}>

                <Modal.Body className="todo-modal">
                    <h4 className='pb-3'>New Todo </h4>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Title</Form.Label>
                        <Form.Control type="text" placeholder="" value={title} onChange={($e) => setTitle($e.target.value)} />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Due Date</Form.Label>
                        <Form.Control type="text" placeholder="" value={dueDate} onChange={($e) => setDueDate($e.target.value)} />
                    </Form.Group>
                    <div className="modal-action">
                        <Button variant="secondary" onClick={submitTodo} className="modal-action-btn btn-save">
                            {isCreate == true ? "Create" : "Save"}
                        </Button>
                        <Button variant="primary" onClick={() => handleClose()} className="modal-action-btn btn-cancel mt-1">
                            Cancel
                        </Button>
                    </div>
                </Modal.Body>
              
            </Modal>
            <h2 className="title">Hi, {userInfo}</h2>
            {listTodo && listTodo.map((todo, index) => {
                return <SingleTodo data={todo} index={index} selectTodo={selectTodo} key={index}></SingleTodo>
            })}
            <div className="btn-add" onClick={() => addTodo()} variant="primary" onClick={handleShow}>
                <FontAwesomeIcon icon={faPlus} />
            </div>
        </>

    )
}



export default Home