import { Card, Button, Badge, Row, Col } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { deleteTodo,updateTodo } from './../actions/todoAction'
import { faTrash } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
const SingleTodo = ({ data, index, selectTodo }) => {
    const dispatch = useDispatch()

    const remove = () => {
        dispatch(deleteTodo(data.title))
    }

    const openModal = () => {
        selectTodo(data,index)
    }
    const done = () => {
        dispatch(updateTodo(index,data.title,data.duedate,"done"))
    }
    return (
        <Card className="mt-2 mb-4 todo" >
            <Card.Body>
                <Row>
                    <Col>
                        <div className="badge" style={{color:data.status =="open"?'#494949':data.status=="done"?'#fff':'#fff',background:data.status =="open"?'#c4c4c4':data.status=="done"?'#39c36d':'#c33939'}}>{data.status}</div>
                    </Col>
                    <Col>
                        <Button variant="danger" onClick={() => remove()} className="btn-trash"><FontAwesomeIcon icon={faTrash} /></Button>
                        {/* <Button variant="danger" onClick={() => openModal()}>Update</Button> */}
                    </Col>
                </Row>
                <h3>{data.title}</h3>
                <Row>
                    <Col md={8} sm={6} xs={6}>
                        <Card.Text>
                            Due Date:
                        </Card.Text>
                        <Card.Text>
                            {data.duedate}
                        </Card.Text>
                    </Col>
                    <Col md={4} sm={6} xs={6}>
                        <Button variant="primary" className="btn-done" onClick={done}>DONE</Button>
                    </Col>
                </Row>
            </Card.Body>
        </Card>
    )
}
export default SingleTodo