export const saveAuth = (user) => {
    return localStorage.setItem("user",user)
}

export const getAuth = () => {
    return localStorage.getItem("user")
}