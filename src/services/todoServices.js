export const saveToDo = (todolist) => {
    return localStorage.setItem("todo",JSON.stringify(todolist))
} 

export const getTodo = () => {
    return localStorage.getItem("todo")? JSON.parse(localStorage.getItem("todo")): []
}