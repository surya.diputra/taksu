import logo from './logo.svg';
import './App.css';
import Login from './component/login';
import { Container, Row, Col } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux'
import Home from './component/home';
function App() {

  const userLogin = useSelector(state => state.userLogin)
  const {loading,error,userInfo} = userLogin

  console.log("userInfolll",userLogin)
  return (
    <div className="App">
      <Container>
        <Row>
          <Col>
            {userInfo != null ? <Home></Home>: <Login></Login>}
            </Col>
        </Row>
      </Container>
    </div>
  );
}

export default App;
