import { USER_LOGIN_SUCCESS } from "../constants/userConstants";
import { getAuth } from "./../services/userServices"
export const userLoginReducer = (state = { loading: false, error: null, userInfo: "" }, action) => {
    switch (action.type) {
        case USER_LOGIN_SUCCESS:
            return { loading: false, userInfo: action.payload.userInfo }
        default:
            return getAuth() ? {
                loading: false,
                error: null,
                userInfo: "romi"
            } : state
    }
}


