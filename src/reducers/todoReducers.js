import { DATA_REMOVE, DATA_CREATE, DATA_UPDATE } from "../constants/todoConstants"
import { getTodo, saveToDo } from "./../services/todoServices"
export const todoReducer = (state = [], action) => {

    switch (action.type) {
        case DATA_CREATE:
            //create data
            state.push({
                status: action.payload.status,
                duedate: action.payload.duedate,
                title: action.payload.title,
            })
            saveToDo(state)
            return state

        case DATA_UPDATE:
            //update data
            var selectedTodoList = [...state]
            if (action.payload.index <= state.length) {
                selectedTodoList[action.payload.index] = {
                    status: action.payload.status,
                    duedate: action.payload.duedate,
                    title: action.payload.title,
                }
                saveToDo(selectedTodoList)
                return selectedTodoList
            } else {
                return state
            }
        case DATA_REMOVE:
            //remove data
            var filteredTodo = state.filter(data => {
                return data.title.toString() != action.payload.toString()
            })
            if (filteredTodo) {
                saveToDo(filteredTodo)
                return filteredTodo
            } else {
                return state
            }
        default:
            return getTodo() ? getTodo() : state
    }
}


